﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Miacid_Lotus.Data
{
    class GameData
    {
        public int numplayers;
        public int currentPlayer;
        public List<Player> players;

        public List<TRule> Rules;
        public List<TMove> Moves;

        GameData()
        {
            
        }
    }
}
