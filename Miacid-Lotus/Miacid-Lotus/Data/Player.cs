﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Miacid_Lotus.SourceFile;

enum PTYE { PLAYER_HUMAN, PLAYER_RULEBASED, PLAYER_STATEBASED };
enum PIECE { PIECE_BAD, PIECE_P1, PIECE_P2,PIECE_P3, PIECE_P4 };

namespace Miacid_Lotus.Data
{
    class Player
    {
        public PIECE piece;
        public bool isPlaying;
        public bool isHuman;
        public bool isRule; // if not, it is state or human
        AbstractAI playerAI; //stores the type of AI the player is, if he is AI

        public Player()
        {
            this.piece = PIECE.PIECE_BAD;
            this.isPlaying = false;
            this.isHuman = true;
            this.isRule = false;
        }
    }
    class PlayerType
    {
        public bool isPlaying;
        public bool isHuman;
        public bool isRule;

        public PlayerType()
        {
            isPlaying = true;
            isHuman = true;
            isRule = false;
        }
    }
}
